# Spring Security JWT

# Pertama

- Akses URL : `http://localhost:8080/authenticate`
- Method : `POST`
- Request Body :

```json
{
  "username": "string",
  "password": "string"
}
```

- Response Body:
```json
{
  "token": "string"
}
```

# Kedua

- Token yang berhasil digenerate sebelumnya, akan digunakan untuk request selanjutnya
- Oleh karena itu, kita tangkap token nya
- Lalu aplikasi kita akan cek, apakah request membawa token atau tidak, apakah tokenya yang dikirimkan di request itu sama dengan token yang berhasil digenerate
- Pengecekan akan menggunakan Filter