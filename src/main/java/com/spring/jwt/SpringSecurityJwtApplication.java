package com.spring.jwt;

import com.spring.jwt.entity.User;
import com.spring.jwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
public class SpringSecurityJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityJwtApplication.class, args);
	}

	@Autowired
	private UserRepository userRepository;

	@PostConstruct
	public void initUsers() {

		List<User> users = Stream.of(
				new User(101, "albert", "albert123", "albert@mail.com"),
				new User(102, "newton", "newton123", "newton@mail.com"),
				new User(103, "tesla", "tesla123", "tesla@mail.com"),
				new User(104, "watt", "watt123", "watt@mail.com"))
				.collect(Collectors.toList());

		userRepository.saveAll(users);
	}

}
